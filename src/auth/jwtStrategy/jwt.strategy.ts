import { Injectable } from "@nestjs/common";
import { PassportStrategy } from "@nestjs/passport";
import { ExtractJwt, Strategy } from 'passport-jwt'


@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {

    constructor() {

        // Configuration for the jwt passport jwt strategy
        super({
            jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(), //jwt from auth header
            ignoreExpiration: false,
            secretOrKey: 'SECRET'  // protected this, move to .env variable file -> sign jwt
        
        });
    
    }

    async validate(payload: any) {

        // const user = await this.userService.getById(payload.sub)

        return {
            id: payload.sub,
            name: payload.name,
            // ...user
        
        };
    
    }

}