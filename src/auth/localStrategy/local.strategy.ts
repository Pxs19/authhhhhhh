import { Injectable, UnauthorizedException } from "@nestjs/common";
import { PassportStrategy } from "@nestjs/passport";
import { Strategy } from "passport-local";
import { AuthService } from "../auth.service";


@Injectable()
export class LocalStrtegy extends PassportStrategy(Strategy) {

    constructor(private authService: AuthService) {
        super({
        
        });  //config ex. facebook, Google -> ClientId, ClientSecret
    
    }

    async validate(username: string, password: string): Promise<any> {

        const user = await this.authService.validateUser(username, password);
        
        // dont have user in database
        if(!user) {
            throw new UnauthorizedException()
        
        }

        return user;
    
    }

    


}