import { Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { UsersModule } from 'src/users/users.module';
import { PassportModule } from '@nestjs/passport';
import { LocalStrtegy } from './localStrategy/local.strategy';
import { SessionSerializer } from './sessionSerializer/session.serializer';
import { JwtModule } from '@nestjs/jwt';
import { JwtStrategy } from './jwtStrategy/jwt.strategy';

@Module({
  imports: [UsersModule, PassportModule, JwtModule.register({
    secret: 'SECRET', // put at .env variables
    signOptions: { expiresIn: '60s' },
  })],
  providers: [AuthService, LocalStrtegy, JwtStrategy],
  exports: [AuthService]
})
export class AuthModule {}
