import { CanActivate, Injectable, ExecutionContext } from '@nestjs/common';

@Injectable()
export class AuthenticatedGuard implements CanActivate {
  // The context and then we are check if its authenticated and is actually something that comes from passpport automatically which is what
//   its going to do assumung you did set up sseions. Its going to try and look for that session and say does the session exits for user -> return true and allow the request


  async canActivate(context: ExecutionContext) {
    const request = await context.switchToHttp().getRequest();

    return request.isAuthenticated();
  }
}
