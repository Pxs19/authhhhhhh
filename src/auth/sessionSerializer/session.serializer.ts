import { Injectable } from "@nestjs/common";
import { PassportSerializer } from "@nestjs/passport";

@Injectable()
export class SessionSerializer extends PassportSerializer {
    serializeUser(user: any, done: (error: Error, user: any) => void ): any {
        // done(null, {id: user.id});

        done(null, user)
    }
    deserializeUser(payload: any, done: (error: Error, payload: string) => void) {
        // ** Can take whatever you get back and return it like that so that a senario
        // where perhaps you again to only save the id to the sesion but on serialize maybe you want to get
        // full information from the database or another api call its up to you **
        // const user = this.userService.findById(pay.id)
        // done(null, user);

        done(null, payload)
    }


}