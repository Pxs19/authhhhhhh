import { Injectable } from '@nestjs/common';

export type User = {
  id: number;
  name: string;
  username: string;
  password: string;
};

@Injectable()
export class UsersService {
  private readonly users: User[] = [
    {
      id: 1,
      name: 'John',
      username: 'Doe',
      password: 'joe',
    },
    {
      id: 2,
      name: 'Mambo',
      username: 'mambo',
      password: 'Dumbo',
    },
  ];

  //   find user by username
  //   they should return user or undefined
  async findOne(username: string): Promise<User | undefined> {
    return this.users.find(user => user.username === username);
  }
}
